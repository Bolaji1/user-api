/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 *
 */
'use strict';

const monitor = require('../utils/monitor');

module.exports = {
    getAllUsers,
    getUserById,
    updateUserById,
    deleteTodoById,
    addUser
};

function getAllUsers(req, res) {
    const start = monitor();

    User.find()
        .exec((err, users) => {
            if (err) {
                res.negotiate(err);
            } else {
                res.json({ users });
                monitor(start, 'getAllUsers');
            }
        })
}

function getUserById(req, res) {
  const start = monitor();

    User.findOne({
            id: parseInt(req.params.user_id)
        })
        .exec((err, user) => {
            if (err) {
                res.negotiate(err);
            } else {
                res.json({ user });
               monitor(start, 'getUserById');
            }
        })
}

function updateUserById(req, res, next) {
    const userId = parseInt(req.params['user_id']);
    const start = monitor();

    if (!userId) res.negotiate(err);

    User.update({
            id: userId
        }, req.body)
        .exec((err, updatedUser) => {
            if (err) {
                res.negotiate(err);
            } else {
                res.json(updatedUser);
                monitor(start, 'updateUserById');
            }
        })
}

function addUser(req, res) {
    const start = monitor();

    User.create({
            forename: req.body.forename,
            surname: req.body.surname,
            email: req.body.email
        })
        .exec((err, newUser) => {
            if (err) {
                res.negotiate(err);
            } else {
                res.json(newUser);
                monitor(start, 'addUser');
            }
        })
}

function deleteTodoById(req, res) {
    const userId = parseInt(req.params['user_id']);
    const start = monitor();

    if (!userId) res.negotiate(err);

    User.destroy({
            id: userId
        })
        .exec((err) => {
            if (err) {
                res.negotiate(err);
            } else {
                res.ok()
                monitor(start, 'deleteTodoById');
            }
        })
}