/**
 * Given some fixtures, bootstrap the data.
 */

const FIXTURES = require('./data');


module.exports = function (cb) {

    createUserFromFiture(cb);
}

function createUserFromFiture(cb){
    const [ userData ] = FIXTURES;

    User.create(userData).exec((err) => {
        if(err){
            console.log('error creating John Doe but Sails will start anyway');
            cb(err)
        }else{
            cb()
        }
    })

}