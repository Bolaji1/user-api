# User API Server built with  [Swagger](https://www.npmjs.com/package/swagger) / [Sails](http://sailsjs.org)

## Task
Create an API to manage a user persistence layer

## Why this Solution
I used both [Swagger](https://www.npmjs.com/package/swagger) and [Sails](http://sailsjs.org) frameworks to complete this
task. Swagger was chosen because of its ease in helping to describing API which is understandable by both technical and 
non-technical personnel. In addition, it forces the developer to document API before coding. Sails under the hood is an 
[Express](https://expressjs.com/) application, that makes it easy to build custom, enterprise-grade Node.js apps. 
It also support data-driven APIs with a scalable, service-oriented architecture. 

## Running this application:
  Clone this repo:  
  `git clone https://Bolaji1@bitbucket.org/Bolaji1/user-api.git`
  
  Change directory into the cloned repo:  
  `cd user-api`  
 
  Run npm install to install the necessary dependencies:  
  `npm install`
  
  Run the app using:  
    `npm run start`
    
    
##To test the API using Swagger Editor:
   Install Swagger globally to enable `swagger` command:      
    `npm install swagger -g`  
   open a new terminal and run:  
    `swagger project edit`