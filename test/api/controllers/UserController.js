const should = require('should');
const request = require('supertest');

describe('controllers', function() {

    describe('UserController', function() {

        describe('GET /', function() {

            it('should return all users in the database', function(done) {

                request(server)
                    .get('/')
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(200)
                    .end(function(err, res) {
                        should.not.exist(err);

                        res.body.should.have.property('users')

                        done();
                    });
            });
        });

        describe('POST /', function() {

            it('should create a new user', function(done) {

                request(server)
                    .post('/')
                    .send({
                        forename: 'Scott',
                        surname: 'Peter',
                        email:'Scott.peter@gamil.com'
                    })
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(200)
                    .end(function(err, res) {
                        should.not.exist(err);
                       res.body.forename.should.be.eql('Scott');
                        done();
                    });
            });

        });

        describe('GET /user/{user_id}', function() {


            it('should get user by id', function(done) {

                request(server)
                    .get('/user')
                    .query({ user_id: 1})
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(200)
                    .end(function(err, res) {
                        should.not.exist(err);
                        done();
                    });
            });

        });

    });

});
